/**
 * Created by Vasyl on 28.12.2016.
 */
$(document).ready(function () {
    ////Форма обратного звонка начало//////////////////
    $("#phone_contact").fancybox({
        padding: 0
    });

    $('#phone_send').on('click', function (even) {
        even.preventDefault();
        if (($('#phone_phone').val().length < 3) && ($('#phone_name').val().length < 3)){
            $('#err_name').show();
            $('#err_phone').show()
        }else if($('#phone_phone').val().length < 3){
            $('#err_phone').show();
            $('#err_name').hide();
        }else if($('#phone_name').val().length < 3){
            $('#err_phone').show();
            $('#err_name').show();
        }else{
        var res = $('#modal_phone_1').serializeArray();
            var arr = {};
            $.each(res, function (result) {
                var $index = res[result].name;
                arr[$index] = res[result].value;
            });
            $.ajax({
                url: 'index.php?route=common/header/contactForm',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function (data) {
                }
            });
            $('#err_name').hide();
            $('#err_phone').hide();
            $('#phone_phone').val('');
            $('#phone_name').val('');
            $.fancybox.close(true);
            swal("Сообщение отправлено", "", "success");
        }
    });
    ////////////Форма обратного звонка конец//////////////
    ////////////Форма написать письмо начало//////////////
    $("#email_contact").fancybox({
        padding: 0
    });
    $('#email_send').on('click', function (even) {
        even.preventDefault();
        if (($('#email_name').val().length < 3) && !($('#email_email').val().match(/^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i)) && ($('#email_massage').val().length < 10)) {
            $('#err_e_name').show();
            $('#err_e_email').show()
            $('#err_e_massage').show()
        }else if(($('#email_name').val().length < 3) && !($('#email_email').val().match(/^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i))) {
            $('#err_e_name').show();
            $('#err_e_email').show()
            $('#err_e_massage').hide()
        }else if(($('#email_name').val().length < 3) && ($('#email_massage').val().length < 10)) {
            $('#err_e_name').show();
            $('#err_e_email').hide()
            $('#err_e_massage').show()
        }else if(!($('#email_email').val().match(/^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i)) && ($('#email_massage').val().length < 10)){
            $('#err_e_name').hide();
            $('#err_e_email').show()
            $('#err_e_massage').show()
        }else if($('#email_name').val().length < 3){
            $('#err_e_name').show();
            $('#err_e_email').hide();
            $('#err_e_massage').hide();
        }else if(!($('#email_email').val().match(/^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i))){
            $('#err_e_name').hide();
            $('#err_e_email').show();
            $('#err_e_massage').hide();
        }else if($('#email_massage').val().length < 10) {
            $('#err_e_name').hide();
            $('#err_e_email').hide();
            $('#err_e_massage').show();
        }else{
            var res = $('#modal_email_1').serializeArray();
            var arr = {};
            $.each(res, function (result) {
                var $index = res[result].name;
                arr[$index] = res[result].value;
            });
            $.ajax({
                url: 'index.php?route=common/header/contactFormEmail',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function (data) {
                }
            });
            $('#err_e_name').hide();
            $('#err_e_email').hide();
            $('#err_e_massage').hide();
            $('#email_name').val('');
            $('#email_email').val('');
            $('#email_massage').val('');
            $.fancybox.close(true);
            swal("Сообщение отправлено", "", "success");
        }
    });
    ////////////Форма написать письмо конец//////////////
    ////////////Форма получить доставку начало//////////////
    $('#delivery_btn_id').on('click', function (even) {
        even.preventDefault();
        if($('#delivery_id').val().length < 3){
            $('#delivery_id').css('border', '1px solid  #ff0c0c');

        }else {
            var res = $('#delivery_id').serializeArray();
            var arr = {};
            $.each(res, function (result) {
                var $index = res[result].name;
                arr[$index] = res[result].value;
            });
            $.ajax({
                url: 'index.php?route=common/header/contactFormDelivery',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function (data) {
                }
            });
            swal("Сообщение отправлено", "", "success");
            $('#delivery_id').val('');
            $('#delivery_id').css('border', '1px solid  #ffe082');
        }
    });
    ////////////Форма получить доставку конец//////////////
    ////////////Форма подписки на акцыи начало//////////////
    $('#special_btn_id').on('click', function (even) {
        even.preventDefault();
        if(!($('#specials_email').val().match(/^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i))){
            $('#specials_email').css('border', '1px solid  #ff0c0c');
        }else {
            var res = $('#subscribe').serializeArray();
            console.log(res);
            var arr = {};
            $.each(res, function (result) {
                var $index = res[result].name;
                arr[$index] = res[result].value;
            });
            $.ajax({
                url: 'index.php?route=common/header/contactFormSpecial',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function (data) {
                }
            });
            swal("Сообщение отправлено", "", "success");
            $('#specials_email').val('');
            $('#specials_email').css('border', '1px solid  #84adbe');
        }
    });
    ////////////Форма подписки на акцыи конец//////////////
    ////////////Форма кцпить в один клик начало//////////////
    $("#one_click_buy_1").fancybox({
        padding: 0
    });

    $('#by-one-click-btn').on('click', function (even) {
        even.preventDefault();
        if (($('#one_name').val().length < 3) && ($('#one_phone').val().length < 3)){
            $('#error_one_click').show();
            $('#one_name').css('border', '1px solid  #ff0c0c');
            $('#one_phone').css('border', '1px solid  #ff0c0c');
        }else if($('#one_name').val().length < 3){
            $('#error_one_click').show();
            $('#one_name').css('border', '1px solid  #ff0c0c');
            $('#one_phone').css('border', '1px solid  #84adbe');
        }else if($('#one_phone').val().length < 3){
            $('#error_one_click').show();
            $('#one_name').css('border', '1px solid  #84adbe');
            $('#one_phone').css('border', '1px solid  #ff0c0c');
        }else{
            var res = $('.one_click_buy_1').serializeArray();
            var arr = {};
            $.each(res, function (result) {
                var $index = res[result].name;
                arr[$index] = res[result].value;
            });
            $.ajax({
                url: 'index.php?route=product/product/contactFormOneClick',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function (data) {
                }
            });
            $('#error_one_click').hide();
            $('#one_name').css('border', '1px solid  #84adbe');
            $('#one_phone').css('border', '1px solid  #84adbe');
            $('#one_name').val('');
            $('#one_phone').val('');
            $.fancybox.close(true);
            swal("Сообщение отправлено", "", "success");
        }
    });
    ////////////Форма кцпить в один клик конец//////////////
});
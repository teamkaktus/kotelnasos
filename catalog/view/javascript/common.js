function getURLVar(key) {
	var value = [];

	var query = String(document.location).split('?');

	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');

			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}

		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	} else { 			// Изменения для seo_url от Русской сборки OpenCart 2x
		var query = String(document.location.pathname).split('/');
		if (query[query.length - 1] == 'cart') value['route'] = 'checkout/cart';
		if (query[query.length - 1] == 'checkout') value['route'] = 'checkout/checkout';
		
		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
}

$(function() {

	$(document).on('click','.buy.modal',function(){
            $('.product_modal_price').html($(this).data('product_price'));
            $('.product_modal_name').html($(this).data('product_name'));
        });
        
	$('.search_opener').click(function(){
		$('.mobile_search').slideToggle(500);
		return false;
	});

	$('.mob_menu').click(function(){
		$('.mobile_mnu').slideToggle(500);
		return false;
	});
	$('.mob_catalog').click(function(){
		$('.mob_cats_wrap').slideToggle(500);
		$('.bg_blue').slideToggle(500);
		return false;
	});
	$('.cat_item_link').on('click', function(){
		var self = $(this),
			self_active = self.hasClass('active');
		
		$('.cat_item_link').next('.submenu').slideUp(200);
		
		if ( self_active ) {
			self.removeClass('active').next('.submenu').slideUp(500);	
		} else {
			self.addClass('active').next('.submenu').slideDown(500);
		}	
		
		return false;
	});

	//SVG Fallback
	if(!Modernizr.svg) {
		$("img[src*='svg']").attr("src", function() {
			return $(this).attr("src").replace(".svg", ".png");
		});
	};

	//Chrome Smooth Scroll
	try {
		$.browserSelector();
		if($("html").hasClass("chrome")) {
			$.smoothScroll();
		}
	} catch(err) {

	};

	$("img, a").on("dragstart", function(event) { event.preventDefault(); });

	//wraping span
	$('.with-decoration').each(function(){
		$(this).wrapInner('<span></span>');
	});

	//home_page corousel
	var clients = $(".logos_carousel");
	clients.owlCarousel({
		items : 4,
		itemsDesktop : [1000,4],
		itemsDesktopSmall : [900,3],
		itemsTablet: [600,2],
		itemsMobile : [500, 1]
	});
	// Custom Navigation Events
	$(".next").click(function(){
		clients.trigger('owl.next');
	});
	$(".prev").click(function(){
		clients.trigger('owl.prev');
	});

	//home_page corousel
	var newtov = $(".tov_slider");
	newtov.owlCarousel({
		items : 6,
		itemsDesktop: [1600,4],
		itemsDesktopSmall : [1000,3],
		itemsTablet: [780,2],
		itemsMobile : [500,1]
	});
	// Custom Navigation Events
	$(".next_item").click(function(){
		newtov.trigger('owl.next');
	})
	$(".next_item_md").click(function(){
		newtov.trigger('owl.next');
	})
	$(".prev_item").click(function(){
		newtov.trigger('owl.prev');
	})
	$(".prev_item_md").click(function(){
		newtov.trigger('owl.prev');
	})

	//cartochka slider
	var cartochka = $(".tovar_gallery");
	cartochka.owlCarousel({
		items : 1,
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1],
		itemsTablet: [600,1],
		itemsMobile : false
	});
	$(".next_slide").click(function(){
		cartochka.trigger('owl.next');
	})
	$(".prev_slide").click(function(){
		cartochka.trigger('owl.prev');
	})

	//chekbox

	function setupLabel() {
		if ($('.label_check input').length) {
			$('.label_check').each(function(){
				$(this).removeClass('c_on');
			});
			$('.label_check input:checked').each(function(){
				$(this).parent('label').addClass('c_on');
			});
		};
		if ($('.label_radio input').length) {
			$('.label_radio').each(function(){
				$(this).removeClass('r_on');
			});
			$('.label_radio input:checked').each(function(){
				$(this).parent('label').addClass('r_on');
			});
		};
	};

	$('.label_check, .label_radio').click(function(){
		setupLabel();
	});
	setupLabel();

	//plus or minus items

	$('.minus').click(function () {
		var $input = $(this).parent().find('input');
		var count = parseInt($input.val()) - 1;
		count = count < 1 ? 1 : count;
		$input.val(count);
		$input.change();
		return false;
	});
	$('.plus').click(function () {
		var $input = $(this).parent().find('input');
		$input.val(parseInt($input.val()) + 1);
		$input.change();
		return false;
	});


	//modal
	$(".modal").fancybox({
		padding: 0
	});


	//tabs
	$(".tabs").lightTabs();

	//accorderon
	var acc = document.getElementsByClassName("accordion");
	var i;

	for (i = 0; i < acc.length; i++) {
		acc[i].onclick = function(){
			this.classList.toggle("active");
			this.nextElementSibling.classList.toggle("show");
		}
	}


	//show more link
	$('a.show_more').click(function(){
		$(".tab_content_wrapper").css("height", "auto")
		$('a.show_more').hide();
		return false;
	});

});

$(document).ready(function() {
	// Highlight any found errors
	$('.text-danger').each(function() {
		var element = $(this).parent().parent();

		if (element.hasClass('form-group')) {
			element.addClass('has-error');
		}
	});

	// Currency
	$('#currency .currency-select').on('click', function(e) {
		e.preventDefault();

		$('#currency input[name=\'code\']').attr('value', $(this).attr('name'));

		$('#currency').submit();
	});

	// Language
	$('#language a').on('click', function(e) {
		e.preventDefault();

		$('#language input[name=\'code\']').attr('value', $(this).attr('href'));

		$('#language').submit();
	});

	/* Search */
        $('#header_search_form input').on('change',function(){
		url = $('base').attr('href') + 'index.php?route=product/search';
		var value = $('#header_search_form input').val();

		if (value) {
			url += '&search=' + encodeURIComponent(value);
		}

		location = url;
        });
        
        $('.mobile_search').on('change',function(){
		url = $('base').attr('href') + 'index.php?route=product/search';

		var value = $('.mobile_search').val();

		if (value) {
			url += '&search=' + encodeURIComponent(value);
		}

		location = url;
        });
        
        
        
//	$('#search input[name=\'search\']').parent().find('button').on('click', function() {
//		url = $('base').attr('href') + 'index.php?route=product/search';
//
//		var value = $('header input[name=\'search\']').val();
//
//		if (value) {
//			url += '&search=' + encodeURIComponent(value);
//		}
//
//		location = url;
//	});
//
//	$('#search input[name=\'search\']').on('keydown', function(e) {
//		if (e.keyCode == 13) {
//			$('header input[name=\'search\']').parent().find('button').trigger('click');
//		}
//	});

	// Menu
	$('#menu .dropdown-menu').each(function() {
		var menu = $('#menu').offset();
		var dropdown = $(this).parent().offset();

		var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

		if (i > 0) {
			$(this).css('margin-left', '-' + (i + 5) + 'px');
		}
	});

	// Product List
	$('#list-view').click(function() {
		$('#content .product-grid > .clearfix').remove();

		//$('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');
		$('#content .row > .product-grid').attr('class', 'product-layout product-list col-xs-12');

		localStorage.setItem('display', 'list');
	});

	// Product Grid
	$('#grid-view').click(function() {
		// What a shame bootstrap does not take into account dynamically loaded columns
		cols = $('#column-right, #column-left').length;

		if (cols == 2) {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
		} else if (cols == 1) {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
		} else {
			$('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
		}

		 localStorage.setItem('display', 'grid');
	});

	if (localStorage.getItem('display') == 'list') {
		$('#list-view').trigger('click');
	} else {
		$('#grid-view').trigger('click');
	}

	// Checkout
	$(document).on('keydown', '#collapse-checkout-option input[name=\'email\'], #collapse-checkout-option input[name=\'password\']', function(e) {
		if (e.keyCode == 13) {
			$('#collapse-checkout-option #button-login').trigger('click');
		}
	});

	// tooltips on hover
	$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});

	// Makes tooltips work on ajax generated content
	$(document).ajaxStop(function() {
		$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
	});
    
    
$('#to_cart').on('click', function (even) {     
     
   
    var phone=document.forms["one_click_buy"]["phone"].value;
    var name=document.forms["one_click_buy"]["name"].value;
    
      if ((phone.length==0) || (name.length==0)){
          document.getElementById("firstnamef").innerHTML="не все поля заполнены";
       
   }else{
       var arr = {
            'name'  : name,
            'phone' : phone
                };
     
    
     
            $.ajax({
                url: 'index.php?route=email/email/send',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function (data) {
                    
                    
                  
                    //$('.alert.alert-success').show();
                    //$('form#contactForm').trigger('reset');
                }
            });
       document.getElementById("firstnamef").innerHTML=""; 
                    document.getElementById("firstname").innerHTML="отправлено";
                    
                    document.forms["one_click_buy"]["phone"].value="";
                    document.forms["one_click_buy"]["name"].value="";
   }

    });
});

// Cart add remove functions
var cart = {
	'add': function(product_id, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();

				if (json['redirect']) {
					location = json['redirect'];
				}

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success style_alert"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close close_style" data-dismiss="alert">&times;</button></div>');

					// Need to set timeout otherwise it wont update the total
					setTimeout(function () {
						$('#cart-total > span').html('' + json['quanty'] + '');
					}, 100);

					$('html, body').animate({ scrollTop: 0 }, 'slow');

					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	},
	'update': function(key, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/edit',
			type: 'post',
			data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	}
}

var voucher = {
	'add': function() {

	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	}
}

var wishlist = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=account/wishlist/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();

				if (json['redirect']) {
					location = json['redirect'];
				}

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success style_alert"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close close_style" data-dismiss="alert">&times;</button></div>');
				}

				$('#wishlist-total span').html(json['total']);
				$('#wishlist-total').attr('title', json['total']);

				$('html, body').animate({ scrollTop: 0 }, 'slow');
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	},
	'remove': function() {

	}
}

var compare = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=product/compare/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success style_alert"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close close_style" data-dismiss="alert">&times;</button></div>');

					$('#compare-total').html(json['total']);

					$('html, body').animate({ scrollTop: 0 }, 'slow');
				}
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	},
	'remove': function() {

	}
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
	e.preventDefault();

	$('#modal-agree').remove();

	var element = this;

	$.ajax({
		url: $(element).attr('href'),
		type: 'get',
		dataType: 'html',
		success: function(data) {
			html  = '<div id="modal-agree" class="modal">';
			html += '  <div class="modal-dialog">';
			html += '    <div class="modal-content">';
			html += '      <div class="modal-header">';
			html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
			html += '      </div>';
			html += '      <div class="modal-body">' + data + '</div>';
			html += '    </div';
			html += '  </div>';
			html += '</div>';

			$('body').append(html);

			$('#modal-agree').modal('show');
		}
	});
});

// Autocomplete */
(function($) {
	$.fn.autocomplete = function(option) {
		return this.each(function() {
			this.timer = null;
			this.items = new Array();

			$.extend(this, option);

			$(this).attr('autocomplete', 'off');

			// Focus
			$(this).on('focus', function() {
				this.request();
			});

			// Blur
			$(this).on('blur', function() {
				setTimeout(function(object) {
					object.hide();
				}, 200, this);
			});

			// Keydown
			$(this).on('keydown', function(event) {
				switch(event.keyCode) {
					case 27: // escape
						this.hide();
						break;
					default:
						this.request();
						break;
				}
			});

			// Click
			this.click = function(event) {
				event.preventDefault();

				value = $(event.target).parent().attr('data-value');

				if (value && this.items[value]) {
					this.select(this.items[value]);
				}
			}

			// Show
			this.show = function() {
				var pos = $(this).position();

				$(this).siblings('ul.dropdown-menu').css({
					top: pos.top + $(this).outerHeight(),
					left: pos.left
				});

				$(this).siblings('ul.dropdown-menu').show();
			}

			// Hide
			this.hide = function() {
				$(this).siblings('ul.dropdown-menu').hide();
			}

			// Request
			this.request = function() {
				clearTimeout(this.timer);

				this.timer = setTimeout(function(object) {
					object.source($(object).val(), $.proxy(object.response, object));
				}, 200, this);
			}

			// Response
			this.response = function(json) {
				html = '';

				if (json.length) {
					for (i = 0; i < json.length; i++) {
						this.items[json[i]['value']] = json[i];
					}

					for (i = 0; i < json.length; i++) {
						if (!json[i]['category']) {
							html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
						}
					}

					// Get all the ones with a categories
					var category = new Array();

					for (i = 0; i < json.length; i++) {
						if (json[i]['category']) {
							if (!category[json[i]['category']]) {
								category[json[i]['category']] = new Array();
								category[json[i]['category']]['name'] = json[i]['category'];
								category[json[i]['category']]['item'] = new Array();
							}

							category[json[i]['category']]['item'].push(json[i]);
						}
					}

					for (i in category) {
						html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

						for (j = 0; j < category[i]['item'].length; j++) {
							html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
						}
					}
				}

				if (html) {
					this.show();
				} else {
					this.hide();
				}

				$(this).siblings('ul.dropdown-menu').html(html);
			}

			$(this).after('<ul class="dropdown-menu"></ul>');
			$(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));

		});
	}
})(window.jQuery);


// update cart
var update_cart = function (key, quantity, id) {
    
    $.ajax({
        url: 'index.php?route=checkout/cart/ajaxEdit',
        type: 'post',
        data: {
            quantity: (typeof(quantity) != 'undefined' ? quantity : 1),
            key: key
        },

       
        beforeSend: function () {
           console.log('beforeSend');
            
        },
        complete: function () {
           console.log('complete');
            
        },
        success: function (json) {
            var tp = json.totals;

			$('#sectioncart').load('index.php?route=checkout/cart/ajaxIndex');

		/*	$("#quanttotal").html(json.count);
			$("#allTotalPrice").html(json.totalPrice);*/


        }
    });
    
};

function quantity_dec(key, id) {
    var q_obj = $("input[name='quantity[" + key + "]']");
    var q_count = q_obj.val();
    console.log(q_count);
    var q = 1;
    if(q_count > 1){
        q = parseInt(q_count) - 1;
        update_cart(key, q, id);
    }




}
function quantity_inc(key, id) {
    var q_obj = $("input[name='quantity[" + key + "]']");
    var q_count = q_obj.val(),
        q = parseInt(q_count) + 1;

    update_cart(key, q, id);
   
}

function quantity_onchange(key, id) {
    
    var q_obj = $("input[name='quantity[" + key + "]']");
    
    
    var q_count = q_obj.val();
        if(q_count==''){q_count=0;}
    console.log('q_obj key: ', q_count);
        q = parseInt(q_count);
    update_cart(key, q, id);
   
}





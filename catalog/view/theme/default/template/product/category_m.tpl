<?php echo $header; ?>

<section>
        <div class="container">
                <div class="row">
                        <div class="col-md-24">
                                <div class="title with-decoration">
                                        <?php echo $heading_title; ?>
                                </div>
                        </div>
                </div>
                <div class="row">
                        <div class="col-md-24">
                                <div class="text">
                                    <?php if ($description) { ?>
                                        <p>
                                            <?php echo $description; ?>
                                        </p>
                                    <?php } ?> 
                                </div>
                        </div>
                </div>
        </div>

</section>

<section>
    <div class="container">
            <div class="row">
                    <div class="col-md-24">
                            <div class="title_line clearfix">
                                    <div class="name_heading">Наименование услуги</div>
                                    <div class="time_heading">Время работы</div>
                                    <div class="price_heading">Стоимость</div>
                            </div>
                            <div class="dop_uslugi">
                                <?php if($products){ ?>
                                    <?php foreach ($products as $product) { ?>
                                        <div class="one_sect clearfix">
                                                <button class="accordion"><?php echo $product['name']; ?></button>
                                                <div class="panel">
                                                    <p><?php echo $product['description']; ?></p>
                                                </div>
                                                <div class="just-wrap">
                                                        <div class="time">
                                                        <?php if ($product['attribute']) { ?>
                                                            <?php foreach($product['attribute'] as $attributes){ ?>
                                                                <?php if($attributes['name'] == 'Монтаж'){ ?>
                                                                    <?php foreach($attributes['attribute'] as $attribute){ ?>
                                                                        <?= $attribute['text']; ?>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        <?php } ?>
                                                        </div>
                                                        <div class="price">
                                                            <?php if ($product['price']) { ?>
                                                                    <?php if (!$product['special']) { ?>
                                                                        <?php echo $product['price']; ?>
                                                                    <?php } else { ?>
                                                                        <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                                                                    <?php } ?>
                                                                    <?php if ($product['tax']) { ?>
                                                                        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                                                    <?php } ?>
                                                            <?php } ?>
                                                        </div>
                                                        <a onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" class="buy">В корзину</a>
                                                </div>

                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                    </div>
            </div>
    </div>
</section>

<?php echo $content_top; ?>
<?php echo $content_bottom; ?>


<?php echo $footer; ?>

<link rel="stylesheet" href="catalog/view/theme/default/stylesheet/search.css" type="text/css"><?php echo $header; ?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" ><?php echo $content_top; ?>
      <h1 class="with-decoration_my"><?php echo $heading_title; ?></h1>
      <!--<label class="control-label" for="input-search"><?php echo $entry_search; ?></label>-->
      <div class="row">
        <div class="col-sm-24" style="margin-bottom: 12px">
          <div class="col-sm-20 search_pole">
          <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control " style="    display: block;
    width: 100%;
    background-color: #fff;
    background-image: url(../img/search_tool.png);
    background-repeat: no-repeat;
    background-position: 35px 10px;
    border: 1px solid #546e7a;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    font-family: 'ProximaNova-ThinItalic';
    font-size: 14px;
    padding-left: 60px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-right: 10px;text-align: left" />
         </div>
          <div class="col-sm-4 search_button ">
           <input  type="button" value="<?php echo $button_search; ?>" id="button-search" style='border:0px solid black;padding-top: 4px;padding-bottom: 4px;' class="btn btn-primary" />
        </div>
          </div>
          <div class="col-sm-24">
          </div>
      </div>
      <?php if ($products) { ?>

  <div class="row">
    <div class="col-xs-24">
      <?php if($products){ ?>
      <?php foreach ($products as $product) { ?>
      <div class="single_hit_tov">
        <figure class="tov_img">
          <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>"  />
        </figure>
        <div class="tov_name">
          <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
        </div>
        <?php if ($product['price']) { ?>
        <div class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-new" ><?php echo $product['special']; ?></span> <span class="price-old"  style="text-decoration: line-through;color:red;"><?php echo $product['price']; ?></span>
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax" ><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
        </div>
        <?php } ?>
        <a href="#" class="to_cart">В корзину</a>
        <div class="info_overlay">
          <a onclick="compare.add('<?php echo $product['product_id']; ?>')" class="compare">сравнить</a>
          <div class="tov_name">
            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
          </div>
          <div class="descript">
            <?php echo $product['description']; ?>
          </div>

          <?php if (!empty($product['stock']) ){ ?>
          <div class="status nalichie">В наличии
          </div>
          <?php } else { ?>
          <div class="">
          </div>
          <?php  } ?>

          <?php if ($product['price']) { ?>
          <div class="price"  >
            <?php if (!$product['special']) { ?>
            <?php echo $product['price']; ?>


            <?php } else { ?>
            <span class="price-new" ><?php echo $product['special']; ?></span> <span class="price-old" style="text-decoration: line-through;color:red;"><?php echo $product['price']; ?></span>

            <?php } ?>
            <?php if ($product['tax']) { ?>
            <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
            <?php } ?>
          </div>
          <?php } ?>
          <a onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" class="to_cart">В корзину</a>
          
          <?php
             $price_h = '';
             if ($product['price']) { ?>
            <?php if (!$product['special']) { ?>
            <?php $price_h = $product['price']; ?>
            <?php } else { ?>
                <?php $price_h = $product['special']; ?>
            <?php } ?>
            <?php } ?>
            <a href="#one_click_buy" data-product_name="<?php echo $product['name']; ?>" data-product_price="<?php echo $price_h; ?>" class="buy modal">Купить в один клик</a>

          
        </div>
      </div>
      <?php } ?>
      <?php } ?>
    </div>
  </div>
        <section class="pagination">
            <div class="container">
                <div class="row">
                    <div class="col-xs-24">
                        <div class="pagination_wrap">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';

	var search = $('#content input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}
/*
	var category_id = $('#content select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}
*/
	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('#content input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});
/*
$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');*/
--></script>
<?php echo $footer; ?>
<?php echo $header; ?>
<section class="manufacturer">
		<div class="container">
			<div class="row">
				<div class="col-xs-24">
					<div class="title with-decoration">
						<?php echo $heading_title; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-24">
					<figure class="baner">
						<img src="<?php echo $image2; ?>" alt="">
					</figure>
					<div class="description">
                        <?php if ($description) { ?>
						<p>
                            <?php echo $description; ?>
						</p>
                        <?php } ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-24">
                                    <div class="tovars">
                                        <?php if($products){ ?>
                                            <?php foreach ($products as $product) { ?>
                                                    <div class="single_hit_tov">
                                                            <figure class="tov_img">
                                                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>"  />
                                                            </figure>
                                                            <div class="tov_name">
                                                                <a href="<?php echo $product['href']; ?>"><span class="type"><?php echo $product['name']; ?></span> 	</a>
                                                            </div>
                                                            <?php if ($product['price']) { ?>
                                                                <div class="price">
                                                                    <?php if (!$product['special']) { ?>
                                                                    <span><?php echo (float)$product['price']; ?></span> РУБ
                                                                    <?php } else { ?>
                                                                    <span class="price-old"  style="text-decoration: line-through;color:red;"><?php echo (float)$product['price']; ?></span> РУБ <span class="price-new"><?php echo (float)$product['special']; ?></span> РУБ
                                                                    <?php } ?>
                                                                    <?php if ($product['tax']) { ?>
                                                                        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                                                    <?php } ?>
                                                                </div>
                                                            <?php } ?>
                                                            <a href="#" class="to_cart">В корзину</a>
                                                            <div class="info_overlay">
                                                                    <a onclick="compare.add('<?php echo $product['product_id']; ?>')" class="compare">сравнить</a>
                                                                    <div class="tov_name">
                                                                            <a href="<?php echo $product['href']; ?>"><span class="type"><?php echo $product['name']; ?></span> 	</a>
                                                                    </div>
                                                                    <div class="descript">
                                                                            <?php echo $product['description']; ?>
                                                                    </div>
                                                                    <div class="status nalichie">В наличии</div>
                                                                    <?php if ($product['price']) { ?>
                                                                        <div class="price">
                                                                            <?php if (!$product['special']) { ?>
                                                                            <span><?php echo (float)$product['price']; ?></span> РУБ
                                                                            <?php } else { ?>
                                                                            <span class="price-old"  style="text-decoration: line-through;color:red;"><?php echo (float)$product['price']; ?></span> РУБ <span class="price-new"><?php echo (float)$product['special']; ?></span> РУБ
                                                                            <?php } ?>
                                                                            <?php if ($product['tax']) { ?>
                                                                                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                                                            <?php } ?>
                                                                        </div>
                                                                    <?php } ?>
                                                                    <a href="#" class="to_cart">В корзину</a>
                                                                    <a href="#one_click_buy" class="buy modal">Купить в один клик</a>
                                                            </div>
                                                    </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                        </div>
</section>
<section class="pagination">
    <div class="container">
        <div class="row">
            <div class="col-xs-24">
                <div class="pagination_wrap">
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
</section>
      <?php echo $content_bottom; ?>
<?php echo $footer; ?>
<?php echo $header; ?>
<?php echo $content_top; ?>
<section class="one_tovar">
        <div class="container">
                <div class="row">
                        <div class="col-md-24">
                                <div class="title with-decoration">
                                        <?php echo $heading_title; ?>
                                </div>
                                <div class="articul">
                                        Артикул: <span><?= $product_id; ?></span>
                                </div>
                        </div>
                </div>
                <div class="row" id="product">
                        <div class="col-md-9 col-sm-12">
                                <div class="gal_wrap">
                                        <div class="tovar_gallery">
                                            <?php if ($thumb || $images) { ?>
                                            <?php if ($thumb) { ?>
                                            <img  src="<?php echo $popup; ?>"/>
                                            <?php } ?>
                                            <?php if ($images) { ?>
                                            <?php foreach ($images as $image) { ?>
                                            <img src="<?php echo $image['thumb']; ?>"/>
                                            <?php } ?>

                                            <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="gal_nav clerfix">
                                                <div class="prev_slide"></div>
                                                <div class="next_slide"></div>

                                        </div>
                                </div>

                        </div>
                        <div class="col-md-14 col-md-offset-1 col-sm-offset-1 col-sm-11">
                                <div class="right_side">
                                        <div class="links">
                                                <div class="nalichie">
                                                        <?php echo $stock; ?>
                                                </div>
                                                <a onclick="compare.add('<?php echo $product_id; ?>');" class="compare">
                                                        Сравнить
                                                </a>
                                        </div>
                                        <div class="tovar_price">
                                            <?php if ($price) { ?>
                                                <?php if (!$special) { ?>
                                                    <span><?php echo $price; ?></span>
                                                <?php } else { ?>
                                                    <span style="text-decoration: line-through;color:red;"><?php echo $price; ?></span>
                                                    <span><?php echo $special; ?></span>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="need_montaj">
                                            <?php if ($options) { ?>
                                                <?php foreach ($options as $option) { ?>
                                                    <?php if ($option['type'] == 'checkbox') { ?>
                                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                            <!--<label class="control-label"><?php echo $option['name']; ?></label>-->
                                                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                                <div class="checkbox">
                                                                    <label class="label_check">
                                                                        <input  type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                                        <?php echo $option_value['name']; ?>
                                                                        <!--<?php if ($option_value['price']) { ?>
                                                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                        <?php } ?>-->
                                                                    </label>
                                                                </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <input type="hidden" value="1" name="quantity">
                                        <input type="hidden" value="<?= $product_id; ?>" name="product_id">
                                        <div class="btns">
                                                <a id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="to_cart">В корзину</a>
                                                <a href="#one_click_buy" class="buy modal">Купить в один клик</a>
                                        </div>
                                        <div class="small_description">
                                            <?php echo $short_description; ?>
                                        </div>
                                        <div class="mini_characteristics">

                                        </div>
                                </div>
                        </div>
                </div>
                <div class="row">
                        <div class="col-md-24">
                                <div class="tabs">
                                        <ul>
                                                <li><span class="dashed">Описание</span></li>
                                                <li><span class="dashed">Характеристики</span></li>
                                                <li><span class="dashed"><?php echo $tab_review; ?></span></li>
                                        </ul>
                                        <div>
                                                <div class="tab_content">
                                                        <div class="tab_content_wrapper">
                                                                <?php echo $description; ?>
                                                        </div>
                                                </div>
                                                <div class="tab_content">
                                                    <?php if ($attribute_groups) { ?>
                                                            <table class="table table-bordered">
                                                                <?php foreach ($attribute_groups as $attribute_group) { ?>
                                                                      <thead>
                                                                            <tr class="str_text">
                                                                                <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                                                                            </tr>
                                                                      </thead>
                                                                      <tbody>
                                                                        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                                                        <tr class="tp_tr">
                                                                          <td class="span_text_spans"><?php echo $attribute['name']; ?></td>
                                                                          <td class="span_text_sps"><?php echo $attribute['text']; ?></td>
                                                                        </tr>
                                                                        <?php } ?>
                                                                      </tbody>
                                                                <?php } ?>
                                                            </table>
                                                    <?php } ?>
                                                </div>
                                                <div class="tab_content">
                                                    <?php if ($review_status) { ?>
                                                        <div class="tab-pane" id="tab-review">
                                                               <form id="form-review">
                                                                

                                                                <div id="review" class="text_reviews"></div>
                                                                <?php if ($review_guest) { ?>
                                                                   <h2 class="text_reviews_title"><?php echo $text_write; ?></h2>
                                                                <div class="form-group required">
                                                                  <div class="col-sm-24">
                                                                    <label class="control-label text_reviews" for="input-name"><?php echo $entry_name; ?></label>
                                                                    <input type="text" name="name" value="" id="input-name" class="form-control pole_reviews" />
                                                                  </div>
                                                                </div>
                                                                <div class="form-group required">
                                                                  <div class="col-sm-24">
                                                                    <label class="control-label text_reviews" for="input-review"><?php echo $entry_review; ?></label>
                                                                    <textarea name="text" rows="5" id="input-review" class="form-control pole_reviews"></textarea>
                                                                    <!--<div class="help-block  text_reviews"><?php echo $text_note; ?></div>-->
                                                                  </div>
                                                                </div>
                                                                <div class="form-group required">
                                                                  <div class="col-sm-24 text_reviews">
                                                                    <label class="control-label "><?php echo $entry_rating; ?></label>
                                                                    &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                                                                    <input type="radio" name="rating" value="1" />
                                                                    &nbsp;
                                                                    <input type="radio" name="rating" value="2" />
                                                                    &nbsp;
                                                                    <input type="radio" name="rating" value="3" />
                                                                    &nbsp;
                                                                    <input type="radio" name="rating" value="4" />
                                                                    &nbsp;
                                                                    <input type="radio" name="rating" value="5" />
                                                                    &nbsp;<?php echo $entry_good; ?></div>
                                                                </div>
                                                                <?php echo $captcha; ?>
                                                                <div class="buttons clearfix">
                                                                  <div class="col-xs-12 reviews_button">
                                                                    <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                                                                  </div>
                                                                </div>
                                                                <?php } else { ?>
                                                                <?php echo $text_login; ?>
                                                                <?php } ?>
                                                            </form>
                                                    <?php } ?>
                                                </div>
                                        </div>            
                                </div> 
                        </div>
                </div>
        </div>
</section>
<section class="hits">
    <div class="container">
        <div class="row">
            <div class="col-xs-24">
                <?php if($products){ ?>
                <?php foreach ($products as $product) { ?>
                <div class="single_hit_tov">
                    <figure class="tov_img">
                        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>"  />
                    </figure>
                    <div class="tov_name">
                        <?php if($product['product_type'] == 'service'){ ?>
                            <a><?php echo $product['name']; ?></a>
                        <?php } else { ?>
                            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                        <?php } ?>
                    </div>
                    <?php if ($product['price']) { ?>
                    <div class="price">
                        <?php if (!$product['special']) { ?>
                        <?php echo $product['price']; ?>
                        <?php } else { ?>
                        <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"  style="text-decoration: line-through;color:red;"><?php echo $product['price']; ?></span>
                        <?php } ?>
                        <?php if ($product['tax']) { ?>
                        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                        <?php } ?>
                    </div>
                    <?php } ?>
                    <a href="#" class="to_cart">В корзину</a>
                    <div class="info_overlay">
                        <a onclick="compare.add('<?php echo $product['product_id']; ?>')" class="compare">сравнить</a>
                        <div class="tov_name">
                            <?php if($product['product_type'] == 'service'){ ?>
                                <a><?php echo $product['name']; ?></a>
                            <?php } else { ?>
                                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                            <?php } ?>
                        </div>
                        <div class="descript">
                            <?php echo $product['description']; ?>
                        </div>
                        <?php if (!empty($product['stock']) ){ ?>
                        <div class="status nalichie">В наличии
                        </div>
                        <?php } else { ?>
                        <div class="">
                        </div>
                        <?php  } ?>
                        <?php if ($product['price']) { ?>
                        <div class="price">
                            <?php if (!$product['special']) { ?>
                            <?php echo $product['price']; ?>
                            <?php } else { ?>
                            <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"  style="text-decoration: line-through;color:red;"><?php echo $product['price']; ?></span>
                            <?php } ?>
                            <?php if ($product['tax']) { ?>
                            <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                            <?php } ?>
                        </div>
                        <?php } ?>
                        <a onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" class="to_cart">В корзину</a>
                        
                        <?php
                             $price_h = '';
                             if ($product['price']) { ?>
                            <?php if (!$product['special']) { ?>
                            <?php $price_h = $product['price']; ?>
                            <?php } else { ?>
                                <?php $price_h = $product['special']; ?>
                            <?php } ?>
                            <?php } ?>
                            <a href="#one_click_buy" data-product_name="<?php echo $product['name']; ?>" data-product_price="<?php echo $price_h; ?>" class="buy modal">Купить в один клик</a>
                        
                    </div>
                </div>
                <?php } ?>
                <?php } ?>

            </div>
        </div>
    </div>
</section>

<?php echo $content_bottom; ?>


<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
				$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('.mini_cart > a > .items').html(json['total_h']);

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > ul').load('index.php?route=common/cart/info ul li');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
   
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
             
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
            console.log(json);
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>
<?php echo $footer; ?>

<?php echo $header; ?>
<div class="container">
  <!--<ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>-->
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="col_np" style="width: "><?php echo $content_top; ?>
      <h1 class="h_style_20"><?php echo $heading_title; ?></h1>
      <?php if ($products) { ?>
        <div>
            <?php foreach ($products as $product) { ?>
            <div class="co_l_4">
                <div>
                    <div class="das">
                        <div>
                            <a class="btr_loc_np" href="<?php echo $product['remove']; ?>">удалить из списка</a>
                        </div>
                        <div class="text-left">

                            <?php if ($product['thumb']) { ?>
                            <img class="img_class_heid" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" />
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div style="margin-left: 3%;margin-right: 10%;">
                    <a href="<?php echo $product['href']; ?>">
                        <div class="div_hei">
                            <strong class="str_text"><?php echo $product['name']; ?></strong>
                        </div>
                    </a>

                    <!--danni-->
                    <!--<td><?php echo $text_price; ?></td>-->
                    <!--<div>
                        <div><?php echo $text_model; ?><?php echo $product['model']; ?></div>
                    </div>
                    <div>
                        <?php echo $text_manufacturer; ?><?php echo $product['manufacturer']; ?>
                    </div>
                    <div>
                        <?php echo $text_availability; ?><?php echo $product['availability']; ?>
                    </div>
                    <div>
                        <p><?php echo $text_summary; ?></p>
                        <span class="description"><?php echo $product['description']; ?></span>
                    </div>
                    <div>
                        <?php echo $text_weight; ?><?php echo $product['weight']; ?>
                    </div>
                    <div>
                        <?php echo $text_dimension; ?>
                        <p><?php echo $product['length']; ?> x <?php echo $product['width']; ?> x <?php echo $product['height']; ?></p>
                    </div>
                    <div>
                        <?php if ($review_status) { ?>
                        <div>
                            <?php echo $text_rating; ?>
                            <div class="rating"><?php for ($i = 1; $i <= 5; $i++) { ?>
                                <?php if ($product['rating'] < $i) { ?>
                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                <?php } else { ?>
                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                <?php } ?>
                                <?php } ?>
                                <br />
                                <?php echo $product['reviews']; ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div>
                        <?php if ($product['price']) { ?>
                        <?php if (!$product['special']) { ?>
                        <?php echo $product['price']; ?>
                        <?php } else { ?>
                        <strike><?php echo $product['price']; ?></strike> <?php echo $product['special']; ?>
                        <?php } ?>

                        <?php } ?>
                    </div>-->
                    <div class="hidden-xs">
                        <?php foreach ($attribute_groups as $attribute_group) { ?>
                        <?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
                        <!--<div colspan="<?php echo count($products) + 1; ?>"><strong><?php echo $attribute_group['name']; ?></strong>
                        </div>-->
                        <div class="col-lg-24 col-md-24 col-sm-24 col-xs-24 no-padding mar_text_tovar">
                            <div class="col-lg-16 col-md-16 col-sm-15 col-xs-16 no-padding">
                                <span class="span_text_span"><?php echo $attribute['name']; ?></span>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-9 col-xs-8 no-padding">
                                <?php if (isset($product['attribute'][$key])) { ?>
                                <span class="span_text_sp"><?php echo $product['attribute'][$key]; ?></span>
                                <?php } else { ?>
                                <span></span>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="hidden-lg hidden-md hidden-sm">
                        <?php foreach ($attribute_groups as $attribute_group) { ?>
                        <?php $i=0 ?>
                        <?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
                        <?php if($i <= 2) { ?>
                        <!--<div colspan="<?php echo count($products) + 1; ?>"><strong><?php echo $attribute_group['name']; ?></strong>
                        </div>-->
                        <div class="col-lg-24 col-md-24 col-sm-24 col-xs-24 no-padding mar_text_tovar">
                            <div class="col-lg-16 col-md-16 col-sm-15 col-xs-16 no-padding">
                                <span class="span_text_span"><?php echo $attribute['name']; ?></span>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-9 col-xs-8 no-padding">
                                <?php if (isset($product['attribute'][$key])) { ?>
                                <span class="span_text_sp"><?php echo $product['attribute'][$key]; ?></span>
                                <?php } else { ?>
                                <span></span>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } else if($i = 3){ ?>
                            <div class="dropdown-menu pad_text1" style="width: 100%;">
                                <div class="col-lg-24 col-md-24 col-sm-24 col-xs-24 no-padding mar_text_tovar">
                                    <div class="col-lg-16 col-md-16 col-sm-15 col-xs-16 no-padding">
                                        <span class="span_text_span"><?php echo $attribute['name']; ?></span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-9 col-xs-8 no-padding">
                                        <?php if (isset($product['attribute'][$key])) { ?>
                                        <span class="span_text_sp"><?php echo $product['attribute'][$key]; ?></span>
                                        <?php } else { ?>
                                        <span></span>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php $i++ ?>
                        <?php } ?>
                        <a  class="hidden-lg hidden-md hidden-sm text dropdown-toggle text_2 pad " data-toggle="dropdown" href="#">
                            <div style="margin-bottom: 5%;">
                                <span class="hiddens spa_text_22">показать все харракетристики</span>
                            </div>
                        </a>

                        <?php } ?>
                    </div>
                    <div>
                        <?php if ($product['price']) { ?>
                        <span class="cena_text"><?php echo $product['price']; ?></span>
                        <?php } ?>
                    </div>
                        <div class="bat_whi">
                            <input type="button" value="<?php echo $button_cart; ?>" class="bt_back_fp" style="font-size: 21px;" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" />
                        </div>
                </div>
            </div>
            <?php } ?>

        </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-default"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
<script>
$(document).ready(function(){
$(".pad").click(function(){
$(".pad_text1").show();
$(".hiddens").hide();
});
});
</script>
<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
<table class="table table-striped table-bordered">
  <tr>
    <td style="width: 30%;"><strong><?php echo $review['author']; ?></strong></td>
    <td colspan="2"><p><?php echo $review['text']; ?></p>
      <?php for ($i = 1; $i <= 5; $i++) { ?>
      <?php if ($review['rating'] < $i) { ?>
      <span class="fa fa-stack" style="color: #E69500"><i class="fa fa-star-o fa-stack-2x"></i></span>
      <?php } else { ?>
      <span class="fa fa-stack" style="color: #E69500"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
      <?php } ?>
      <?php } ?></td>
  </tr>
</table>
<div style="text-align: right">
  <span class="text-right" style="text-align: right"><?php echo $review['date_added']; ?></span>
</div>
<?php } ?>
<section class="pagination">
  <div class="container">
    <div class="row">
      <div class="col-xs-24">
        <div class="pagination_wrap">
          <?php echo $pagination; ?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<?php } ?>

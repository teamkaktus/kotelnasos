<?php echo $header; ?>
<?php if ($attention) { ?>
  <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>

  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
<section id="sectioncart" class="cart">
		<div class="container">
			<div class="row">
				<div class="col-md-24">
					<div class="title with-decoration">
						<?php echo $heading_title; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-24">
					<div id="table_cart" class="table_cart">
						<!-- <table>
							<thead>
								<tr>
									<td></td>
									<td>Наименование</td>
									<td>Стоимость</td>
									<td>Кол-во</td>
									<td>Сумма</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<figure class="tov_img">
											<img src="img/cart_image.png" alt="">
										</figure>
									</td>
									<td>
										<div class="articul">
											Код товара: <span>15089725</span>
										</div>
										<div class="tov_name">
											<span>Скважинный насос БЕЛАМОС SP 40/5</span>
										</div>
									</td>
									<td>
										<div class="skidka">Скидка: <span>920 р.</span></div>
										<div class="you_price">Ваша цена: <span>8 277 р.</span></div>
									</td>
									<td>
										<div class="number">
										<span class="minus"></span>
											<input type="text" value="1" size="5"/>
											<span class="plus"></span>
										</div>
									</td>
									<td>
										<div class="clearfix">
											<div class="amount"><span>8 277 р</span></div>
											<a href="#" class="remove"></a>
										</div>
										
									</td>
								</tr>
								<tr class="spacer"></tr>
								<tr>
									<td>
										<figure class="tov_img">
											<img src="img/cart_image.png" alt="">
										</figure>
									</td>
									<td>
										<div class="articul">
											Код товара: <span>15089725</span>
										</div>
										<div class="tov_name">
											<span>Скважинный насос БЕЛАМОС SP 40/5</span>
										</div>
									</td>
									<td>
										<div class="skidka">Скидка: <span>920 р.</span></div>
										<div class="you_price">Ваша цена: <span>8 277 р.</span></div>
									</td>
									<td>
										<div class="number">
										<span class="minus"></span>
											<input type="text" value="1" size="5"/>
											<span class="plus"></span>
										</div>
									</td>
									<td>
										<div class="clearfix">
											<div class="amount"><span>8 277 р</span></div>
											<a href="#" class="remove"></a>
										</div>
										
									</td>
								</tr>
								<tr class="spacer"></tr>
								<tr>
									<td>
										<figure class="tov_img">
											<img src="img/cart_image.png" alt="">
										</figure>
									</td>
									<td>
										<div class="articul">
											Код товара: <span>15089725</span>
										</div>
										<div class="tov_name">
											<span>Скважинный насос БЕЛАМОС SP 40/5</span>
										</div>
									</td>
									<td>
										<div class="skidka">Скидка: <span>920 р.</span></div>
										<div class="you_price">Ваша цена: <span>8 277 р.</span></div>
									</td>
									<td>
										<div class="number">
										<span class="minus"></span>
											<input type="text" value="1" size="5"/>
											<span class="plus"></span>
										</div>
									</td>
									<td>
										<div class="clearfix">
											<div class="amount"><span>8 277 р</span></div>
											<a href="#" class="remove"></a>
										</div>
										
									</td>
								</tr>
								<tr class="spacer"></tr>
							</tbody>
						</table> -->
						<div class="cart_table_row hidden-xs hidden-sm">
							<div class="table_cell"></div>
							<div class="table_cell">Наименование</div>
							<div class="table_cell">Стоимость</div>
							<div class="table_cell">Кол-во</div>
							<div class="table_cell">Сумма</div>
						</div>
						 <?php foreach ($products as $product) { ?>
						<div class="cart_table_row clearfix">
							<div class="table_cell">
								<figure class="tov_img">
									<img src="<?php echo $product['thumb']; ?>" alt="">
								</figure>
							</div>
							<div class="table_cell">
								<div class="articul">
									Код товара: <span><?php echo $product['product_id']; ?></span>
								</div>
								<div class="tov_name">
									<span><?php echo $product['name']; ?></span>
								</div>
							</div>
							<div class="table_cell">
								<div class="skidka">Скидка: <span><?php echo $product['special_total']; ?> р.</span></div>
								<div class="you_price">Ваша цена: <span><?php echo $product['price']; ?></span></div>
							</div>
							<div class="table_cell">
								<div class="number">
									<span class="minus" onclick="quantity_dec('<?=$product['cart_id'];?>', <?=$product['product_id'];?>)"></span>
									<input type="text" name="quantity[<?php echo $product['cart_id']; ?>]"
												   value="<?php echo $product['quantity']; ?>" size="5" oninput="quantity_onchange('<?=$product['cart_id'];?>', <?=$product['product_id'];?>)"/>
									<span class="plus" onclick="quantity_inc('<?=$product['cart_id'];?>', <?=$product['product_id'];?>)"></span>
								</div>
							</div>
							<div class="table_cell">
								<div class="clearfix sum">
									<div class="amount"><span><?php echo $product['total']; ?></span></div>
									<a href="#" class="remove" onclick="cart.remove('<?php echo $product['cart_id']; ?>');"></a>
								</div>
							</div>
						</div>
 <?php } ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-24">
					<div class="cart_info clearfix">
						<div class="row">
							<div class="col-md-12  col-sm-24">
								<div class="info">
									<ul>
										<li>Общая скидка <span><?php echo $product_total_spesial; ?> р.</span></li>
										<li>Общее кол-во товаров: <span id="quanttotal"><?php echo $product_total_count; ?></span> шт</li>
										<li>Общая стоимость: <span id="allTotalPrice"><?php echo $totals['0']['text']; ?></span></li>
										 
            
            
									</ul>
								</div>
							</div>
							<div class="col-md-12 col-sm-24">
								<div class="links">
									<a href="#one_click_buy" class="buy modal">Купить в один клик</a>
									<a href="<?php echo $checkout; ?>" class="to_cart">Оформить заказ</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
					<div class="hidden">
		<div id="one_click_buy">
			<div class="one_click">
				<div class="heading">
				
					<span>Покупка в один клик</span>						
				</div>
                <form id="one_click_buy">
					Запполните данные, <br>
					и наш менеджер свяжется с вами!
					<input type="text" name="name" placeholder="Ваше имя">
					<input type="text" name="phone" placeholder="Ваш телефон">
					<span style="color:red" id="firstnamef"></span>
					<span style="color:#0062ff" id="firstname"></span>
					<button type="button" id="to_cart" class="to_cart">Отправить</button>
					<div class="summary">
						В корзине <span class="quanty"><?php echo $product_total; ?></span> товара на сумму: <span class="summa"><?php echo $totals['0']['text']; ?></span>
					</div>
				</form>
			</div>
		</div>
	</div>
	</section>
<?php echo $footer; ?>

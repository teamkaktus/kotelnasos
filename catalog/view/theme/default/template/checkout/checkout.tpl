<?php echo $header; ?>
    <section id="delivery_method" class="delivery_method" style="display: block;">
		<div class="container">
			<div class="row">
				<div class="col-md-24">
					<div class="title with-decoration">
						Выберите способ доставки
					</div>
				</div>
			</div>
			<form id="delivery_method">
				<fieldset class="radios">			
					<div class="row">
						<div class="col-md-8 col-sm-12">
						<span style="color:red" id="firstnamef"></span>
							<label id="deliverys" class="label_radio r_on" for="delivery"><input name="sample-radio" id="delivery" value="delivery" type="radio" checked="" >Доставка</label>
							<input type="text" name="name" placeholder="Ваше имя" >
							<input type="text" name="phone" placeholder="Ваш телефон" >
							<input type="text" name="adress" placeholder="Ваш адрес" >
							<input type="email" name="email" placeholder="Ваш e-mail" >					
						</div>
						<div class="col-md-offset-1 col-md-8 col-sm-11 col-sm-offset-1">							
							<label class="label_radio" for="local"><input name="sample-radio" id="local" value="local" type="radio">Самовывоз
								<span>г.Москва, ул. Чайковского, д. 58А</span>
							</label>				
						</div>
					</div>
					<div class="row">
						<div class="col-md-24">
							<button id="nextsteap" class="confirm_btn" type="button">Перейти к подтверждению заказа </button>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</section>

<section id="submit_delivery" class="submit_delivery" style="display: none;">
		<div class="container">
			<div class="row">
				<div class="col-md-24">
					<div class="with-decoration title">
						Подтвердите доставку
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-24">
					<div class="table_wraper">
						<table>
							<tbody>
								<tr>
									<th>Данные заказа</th>
									<th></th>
								</tr>
								<tr>
									<td class="name">Ваш заказ</td>
									<td><?php echo $product_total; ?> товара на сумму <span class="amount"><?php echo $price_total; ?></span> р.</td>
								</tr>
								<tr>
									<td class="name">Способ получения</td>
									<td><span id="sposobdost">Самовывоз из офиса: ул. Чайковского, д. 58А</span></td>
								</tr>
								<tr>
									<td class="name">Дата получения</td>
									<td><span id="">2 декабря, после 17:00</span></td>
								</tr>
								<tr>
									<td class="name">Способ оплаты</td>
									<td><span id="sposobopl">Оплата наличными в магазине</span></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-24">
					<div class="table_wraper">
						<table>
							<tbody>
								<tr>
									<th>Получатель заказа</th>
									<th></th>
								</tr>
								<tr>
									<td class="name">ФИО</td>
									<td><span id="name">Судьин Дмитрий не указано</span></td>
								</tr>
								<tr>
									<td class="name">Телефон</td>
									<td><span id="phone">+7 (912) 033-00-55</span></td>
								</tr>
								<tr>
									<td class="name">Email</td>
									<td><span id="email">sudin.d.e@mail.ru</span></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-24">
					<div class="confirm"> <span style="color:red" id="label_checkinfo"></span>
						<label id="label_checks" class="label_check" for="confirm_delivery"><input name="confirm_delivery" id="confirm_delivery" value="no" type="checkbox" >Я подтверждаю заказ</label>

					</div>
					<div class="confirm_btn_wrap">
							<a href="" id="button-register"
							   class="confirm_btn">
								Подтвердить заказ 
							</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	
<script type="text/javascript"><!--
    
    $(document).ready(function() {
    
$('#nextsteap').on('click', function (even) {     
     

    var name=document.forms["delivery_method"]["name"].value;
    var phone=document.forms["delivery_method"]["phone"].value;
    var adress=document.forms["delivery_method"]["adress"].value;
    var email=document.forms["delivery_method"]["email"].value; 
    
    
    
    
    var delivery=  document.getElementById("deliverys");
    
      if ((name.length==0) || (phone.length==0) || (adress.length==0) || (email.length==0)){
          document.getElementById("firstnamef").innerHTML="не все поля заполнены";
       
   }else{
          
       document.getElementById('submit_delivery').style.display = 'block'; 
       document.getElementById('delivery_method').style.display = 'none';
       
       $("#name").html(name);
       $("#phone").html(phone);
       $("#email").html(email);
       if(delivery.className=='label_radio r_on'){
           sposobdost='Доставка';
           sposobopl='Оплата при доставке';
       }else{
           sposobdost='Самовывоз из офиса: ул. Чайковского, д. 58А';
           sposobopl='Оплата наличными в магазине';
       }
       $("#sposobdost").html(sposobdost);
     

   }

});

});
    
	// Register
	$(document).delegate('#button-register', 'click', function()
	{
        var delivery=  document.getElementById("deliverys");
        var label_checks=  document.getElementById("label_checks");
         if(label_checks.className=='label_check'){
           document.getElementById("label_checkinfo").innerHTML="вы должны подтвердить";
             return false;
       }else{
        if(delivery.className=='label_radio r_on'){
           sposobdost='Доставка';
           sposobopl='Оплата при доставке';
       }else{
           sposobdost='Самовывоз из офиса: ул. Чайковского, д. 58А';
           sposobopl='Оплата наличными в магазине';
       }
		var payment_method = sposobdost;

           
            var name=document.forms["delivery_method"]["name"].value;
            var phone=document.forms["delivery_method"]["phone"].value;
            var adress=document.forms["delivery_method"]["adress"].value;
            var email=document.forms["delivery_method"]["email"].value; 
		
         var data = {
            'name'  : name,
            'phone' : phone,
            'adress': adress,
            'email' : email,
            'payment' : payment_method
             
                };
     
		
console.log(data);
		$.ajax({
			url: 'index.php?route=checkout/confirm',
			type: 'post',
			data: data,
			dataType: 'json',
			beforeSend: function() {
				$('#button-register').button('loading');
			},
			complete: function() {
				$('#button-register').button('reset');
			},
			success: function() {
					location = 'index.php?route=checkout/success';
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		}); 
            
       }
	});
    
//--></script>
<?php echo $footer; ?>
<?php echo $header; ?>

        <?php echo $content_top; ?>
<section class="clients_corousel" id="clients_corousel_id">
        <div class="container">
                <div class="row">
                        <div class="col-md-1 col-xs-3 col-sm-1">
                                <div class="customNav">						
                                        <div class="prev"></div>
                                </div>
                        </div>
                        <div class="col-md-22 col-xs-18 col-sm-22">
                                <div class="logos_carousel">
                                    <?php foreach ($manufacturers as $manufacturer) { ?>
                                        <div class="one_client">
                                                <a href="<?php echo $manufacturer['link']; ?>">
                                                    <img src="<?php echo $manufacturer['image']; ?>" alt="">
                                                </a>
                                        </div>
                                    <?php } ?>
                                        
                                </div>
                        </div>
                        <div class="col-md-1 col-xs-3 col-sm-1">
                                <div class="customNav">						
                                        <div class="next"></div>
                                </div>
                        </div>
                </div>
        </div>
</section>
<section class="categ">
        <div class="container">
                <div class="row">
                        <div class="col-md-24">
                                <div class="cat_items">
                                        <ul class="cat_wrap">
                                                <?php foreach($categories as $category){ ?>
                                                <li class="cat_item">
                                                        <a href="#" class="cat_item_link"><img style="margin-right: 10px" src="<?= $category['image']; ?>"><?= $category['name']; ?></a>
                                                        <ul class="submenu">
                                                                <?php foreach ($category['children'] as $child){ ?>
                                                                <li class="item">
                                                                        <a href="<?= $child['href']; ?>" class="category_link">
                                                                                <?= $child['name']; ?>
                                                                        </a>
                                                                </li>
                                                                <?php } ?>
                                                        </ul>
                                                </li>
                                                <?php } ?>
                                        </ul>

                                </div>
                        </div>
                </div>
        </div>
</section>
        <?php echo $content_bottom; ?>


        
<?php echo $footer; ?>
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>


<meta charset="utf-8">
<title>Все для отоплния и водоснабжения</title>
<meta name="description" content="">

<link rel="shortcut icon" href="/img/favicon/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="/img/favicon/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-touch-icon-114x114.png">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">



<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min_1.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="catalog/view/javascript/animate/animate.css" type="text/css">
<link rel="stylesheet" href="catalog/view/javascript/owl-carousel/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="catalog/view/javascript/fancybox/jquery.fancybox.css" type="text/css">
<link rel="stylesheet" href="catalog/view/theme/default/stylesheet/fonts.css" type="text/css">
<link rel="stylesheet" href="catalog/view/theme/default/stylesheet/main.css" type="text/css">
<link rel="stylesheet" href="catalog/view/theme/default/stylesheet/media.css" type="text/css">
<link rel="stylesheet" href="catalog/view/theme/default/stylesheet/Reviews.css" type="text/css">
<link rel="stylesheet" href="catalog/view/theme/default/stylesheet/mobile_styles.css" type="text/css">
<link rel="stylesheet" href="catalog/view/theme/default/stylesheet/tabs.css" type="text/css">
<link rel="stylesheet" href="catalog/view/theme/default/stylesheet/search.css" type="text/css">

<link rel="stylesheet" href="catalog/view/theme/default/stylesheet/vars.css" type="text/css">

<script src="catalog/view/javascript/modernizr/modernizr.js"></script>
<script src="catalog/view/javascript/waypoints/waypoints.min.js"></script>
<script src="catalog/view/javascript/animate/animate-css.js"></script>
<script src="catalog/view/javascript/owl-carousel/owl.carousel.min.js"></script>
<script src="catalog/view/javascript/plugins-scroll/plugins-scroll.js"></script>
<script src="catalog/view/javascript/fancybox/jquery.fancybox.pack.js"></script>
<script src="catalog/view/javascript/tabs/tabs.js"></script>

<script src="catalog/view/javascript/contact-form.js"></script>




        <!--<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet"> -->
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">

<header class="<?php if($ishome == 'no'): echo 'inner_page'; endif;?>">
    <div class="container">
            <div class="row">
                    <div class="site_info">
                            <div class="col-md-8 col-sm-8">
                                    <div class="logo_wrap">
                                        <?php if ($logo) { ?>
                                            <a href="<?php echo $home; ?>">
                                                <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="logo" />
                                            </a>
                                        <?php } ?>
                                    </div>
                            </div>
                            <div class="col-xs-24 hidden-sm hidden-md hidden-lg">
                                    <div class="mobile_items clearfix">
                                            <div class="mob_menu_btn">
                                                    <a href="#" class="mob_menu">МЕНЮ</a>
                                            </div>
                                            <div class="butuns">
                                                    <div class="mobile_cart">
                                                            <a href="<?= $carthref; ?>"><span><?= $cart; ?></span></a>
                                                    </div>
                                                    <div class="mob_search">
                                                            <button class="search_opener"></button>
                                                            <input type="text" class="mobile_search" placeholder="Что ищем?">
                                                    </div>
                                                    <a href="#" class="tel"></a>
                                                    <a href="#" class="mail"></a>
                                            </div>
                                    </div>
                            </div>
                            <div class="col-md-offset-1 hidden-xs col-md-9 col-sm-9">
                                    <div class="row">
                                            <div  class="col-md-14 col-sm-24">
                                                    <a id="phone_contact" href="#modal_phone" class="call_back" >
                                                            Заказать обратный звонок
                                                    </a>
                                            </div>
                                            <div class="col-md-10 col-sm-24">
                                                    <a id="email_contact" href="#modal_email" class="write_us">
                                                            Написать письмо
                                                    </a>
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-md-24 col-sm-24">
                                                    <div id="header_search_form">
                                                            <input type="text" name="search" placeholder="Поиск по сайту...">
                                                    </div>
                                            </div>
                                    </div>
                            </div>
                            <div class="col-md-offset-1 hidden-xs col-md-5 col-sm-7 col-sm-offset-0 ">
                                    <div class="row">
                                            <div class="col-md-24 col-sm-24">
                                                    <div class="phone_wrap">
                                                        <a href="tel:<?= $telephone; ?>"><?= $telephone; ?></a>
                                                    </div>
                                            </div>

                                            <div class="col-md-24 col-sm-24">
                                                    <div class="mini_cart">
                                                            <a href="<?= $carthref; ?>">
                                                                    В корзине:
                                                                    <span class="items"><?= $cart; ?></span>
                                                                    <span class="tovs">товаров</span>
                                                            </a>
                                                    </div>
                                            </div>
                                    </div>
                            </div>
                    </div>
            </div>
    </div>
    <div class="mobile_mnu">
            <div class="triangle"></div>
            <div class="bg_blue">
                    <div class="container">
                            <div class="row">
                                    <div class="col-md-24">
                                            <nav class="mobile_nav">
                                                    <ul>
                                                            <li class="mob_menu_item">
                                                                    <a href="index.php?route=information/information&information_id=4" class="mob_menu_link">О компании </a>
                                                            </li>
                                                            <li class="mob_menu_item">
                                                                    <a href="#" class="mob_menu_link">Производители</a>
                                                            </li>
                                                            <li class="mob_menu_item">
                                                                    <a href="index.php?route=information/information&information_id=7" class="mob_menu_link">Акции</a>
                                                            </li>
                                                            <li class="mob_menu_item">
                                                                    <a href="index.php?route=information/information&information_id=6" class="mob_menu_link">Доставка</a>
                                                            </li>
                                                            <li class="mob_menu_item">
                                                                    <a href="index.php?route=information/information&information_id=8" class="mob_menu_link">Оплата</a>
                                                            </li>
                                                            <li class="mob_menu_item">
                                                                <a href="../../../index.php?route=product/category&path=1" class="mob_menu_link">Монтаж</a>
                                                            </li>
                                                    </ul>
                                            </nav>
                                    </div>
                            </div>
                    </div>
            </div>
            <div class="bg_yellow">
                    <div class="container">
                            <div class="row">
                                    <div class="col-md-24">
                                            <a href="#" class="mob_catalog">Каталог</a>

                                    </div>
                            </div>
                    </div>
                    <ul class="mob_cats_wrap">

                            <li class="mobile_category_item">
                                    <?php foreach($categories as $category){ ?>
                                        <a href="<?= $category['href']; ?>" class="mob_category_link">
                                                <?= $category['name']; ?>
                                        </a>
                                    <?php } ?>
                            </li>
                    </ul>
            </div>
    </div>
    <div class="home_branding">
            <div class="container">
                    <div class="row">
                            <div class="col-md-24">
                                    <nav class="main_menu">
                                            <ul class="main_menu_wrap">
                                                    <li class="catalog_menu_cats">
                                                            <a class="catalog_menu_parent" href="#">Каталог</a>
                                                            <ul class="categories_list">
                                                                <?php foreach ($categories as $category){ ?>
                                                                <?php foreach ($category['children'] as $child){ ?>
                                                                <li class="category_item">
                                                                    <a href="<?= $child['href']; ?>" class="category_link">
                                                                        <?= $child['name']; ?>
                                                                    </a>
                                                                </li>
                                                                <?php } ?>
                                                                <?php } ?>
                                                            </ul>
                                                    </li>
                                                    <li class="main_menu_item">
                                                            <a href="index.php?route=information/information&information_id=4" class="main_menu_item_link">О компании </a>
                                                    </li>
                                                    <li class="main_menu_item">
                                                            <a href="#clients_corousel_id" class="main_menu_item_link">Производители</a>
                                                    </li>
                                                    <li class="main_menu_item">
                                                            <a href="index.php?route=information/information&information_id=7" class="main_menu_item_link">Акции</a>
                                                    </li>
                                                    <li class="main_menu_item">
                                                            <a href="index.php?route=information/information&information_id=6" class="main_menu_item_link">Доставка</a>
                                                    </li>
                                                    <li class="main_menu_item">
                                                            <a href="index.php?route=information/information&information_id=8" class="main_menu_item_link">Оплата</a>
                                                    </li>
                                                    <li class="main_menu_item">
                                                            <a href="../../../index.php?route=product/category&path=1" class="main_menu_item_link">Монтаж</a>
                                                    </li>
                                            </ul>
                                    </nav>
                            </div>
                    </div>
                    <?php if($ishome == 'yes'){ ?>
                        <div class="row">
                                <div class="col-md-11 col-xs-24">
                                        <div class="gift_wraper">
                                                <span class="with-decoration">
                                                        Дарим бесплатную
                                                </span>
                                                <br>
                                                <span class="with-decoration">
                                                        доставку до объекта!
                                                </span>
                                        </div>

                                        <form class="gift_form">
                                                <div class="row">
                                                        <div class="col-lg-11 col-md-14 col-xs-16">
                                                                <input id="delivery_id" type="text" name="phone" placeholder="Ваш номер телефона">
                                                        </div>
                                                        <div class="col-lg-9 col-md-10 col-xs-16">
                                                                <button id="delivery_btn_id" type="submit">Получить доставку</button>
                                                        </div>

                                                </div>
                                        </form>
                                </div>
                        </div>
                    <?php } ?>
            </div>
    </div>
</header>
<div class="hidden">
        <div id="modal_phone">
                <div class="modal_phone ">
                        <div class="heading">
                            <span>Заявка на обратный звонок</span>
                        </div>
                        <div class="row" style="margin: 0; padding-left: 10px">
                            <div>
                                <span id="err_name" style="display: none; color: red;">Некоректно заполненно имя</span>
                            </div>
                            <div>
                                <span id="err_phone" style="display: none; color: red">Некоректно введен телефон</span>
                            </div>
                        </div>
                        <form id="modal_phone_1">
                                Запполните данные, <br>
                                и наш менеджер свяжется с вами!
                                <input id="phone_name" type="text" name="phone_name" placeholder="Ваше имя">
                                <input id="phone_phone" type="text" name="phone_phone" placeholder="Ваш телефон">
                                <button id="phone_send" type="submit" class="phone_send">Отправить</button>
                        </form>
                </div>
        </div>
</div>
<div class="hidden">
        <div id="modal_email">
                <div class="modal_email ">
                        <div class="heading">
                                <span>Написать письмо</span>
                        </div>
                        <div class="row" style="margin: 0; padding-left: 10px">
                            <div>
                                <span id="err_e_name" style="display: none; color: red;">Некоректно заполненно имя</span>
                            </div>
                            <div>
                                <span id="err_e_email" style="display: none; color: red">Некоректно введен E-mail</span>
                            </div>
                            <div>
                                <span id="err_e_massage" style="display: none; color: red">Некоректно введенно сообщение</span>
                            </div>
                        </div>
                        <form id="modal_email_1">
                                Запполните данные, <br>
                                и наш менеджер свяжется с вами!
                                <input id="email_name" type="text" name="email_name" placeholder="Ваше имя">
                                <input id="email_email" type="text" name="email_email" placeholder="Ваш E-mail">
                                <input id="email_massage" type="text" name="email_massage" placeholder="Ваше сообщение">
                                <button id="email_send" type="submit" class="email_send">Отправить</button>
                        </form>
                </div>
        </div>
</div>
<div>
        <div id="content"></div>
</div>

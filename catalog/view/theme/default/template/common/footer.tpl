
<section>
        <div class="container">
                <div class="row">
                        <div class="col-md-24">
                                <div class="watch_video">
                                        Смотрите видеоотзывы
                                        <a href="<?php echo $config_reviews; ?>"><img src="img/play.png" alt="Play"></a>
                                        о нашей компании
                                </div>
                        </div>
                </div>
        </div>
</section>
<section>
        <?php if(!empty($data['modules'][0])){ ?>
        <?php echo $data['modules'][0]; ?>
        <?php }?>
</section>
<section class="all_cats">
        <div class="container">
                <div class="row">
                        <div class="col-md-6">
                                <div class="all_cat_title">
                                        Каттегории товаров
                                </div>
                        </div>
                        <div class="col-md-18">
                                <ul class="cats_list">
                                        <?php foreach ($categories as $category){ ?>
                                        <?php foreach ($category['children'] as $child){ ?>
                                        <li class="single_cat">
                                                <a href="<?= $child['href']; ?>" class="cat_link">
                                                        <?= $child['name']; ?>
                                                </a>
                                        </li>
                                        <?php } ?>
                                        <?php } ?>
                                </ul>
                        </div>
                </div>
        </div>
</section>
<footer>
        <div class="container">
                <div class="row hidden-xs">
                        <div class="col-md-24">
                                <nav class="footer_menu">
                                        <ul class="f_menu_wrap">
                                                <li class="f_menu_item"><a href="index.php?route=information/information&information_id=4" class="f_menu_link">О компании</a></li>
                                                <li class="f_menu_item"><a href="#clients_corousel_id" class="f_menu_link">Производители</a></li>
                                                <li class="f_menu_item"><a href="index.php?route=information/information&information_id=7" class="f_menu_link">Акции</a></li>
                                                <li class="f_menu_item"><a href="index.php?route=information/information&information_id=6" class="f_menu_link">Доставка</a></li>
                                                <li class="f_menu_item"><a href="index.php?route=information/information&information_id=8" class="f_menu_link">Оплата</a></li>
                                                <li class="f_menu_item"><a href="#" class="f_menu_link">Контакты</a></li>
                                        </ul>
                                </nav>
                        </div>
                </div>
                <div class="row branding">
                        <div class="col-lg-16 col-md-12 col-sm-12">
                                <figure>
                                        <a href="#">
                                                <img src="img/footer_logo.png" alt="Logo">
                                        </a>
                                </figure>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12">
                                <div class="info clearfix">
                                        <img src="img/carts.png" alt="Carts">
                                        <a id="phone_contact" href="#modal_phone" class="call_back" >Заказать звонок</a>
                                </div>
                        </div>
                </div>
        </div>
</footer>
<!--<footer>
  <div class="container">
    <div class="row">
      <?php if ($informations) { ?>
      <div class="col-sm-3">
        <h5><?php echo $text_information; ?></h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-3">
        <h5><?php echo $text_service; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
          <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_extra; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
          <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
      </div>
    </div>
    <hr>
    <p><?php echo $powered; ?></p>
  </div>
</footer>-->

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->
<div class="hidden">
        <div id="one_click_buy">
                <div class="one_click">
                        <div class="heading">
                                <span>Покупка в один клик</span>
                        </div>
                        <form id="one_click_buy" class="one_click_buy_1">
                                Запполните данные, <br>
                                и наш менеджер свяжется с вами!
                                <span id="error_one_click" style="display: none; color: red">Некорректно заполнены данные</span>
                                <input id="product_name" name="product_name" type="hidden" value="<?php echo $heading_title; ?>">
                                <input id="one_name" type="text" name="name" placeholder="Ваше имя">
                                <input id="one_phone" type="text" name="phone" placeholder="Ваш телефон">
                                <button id="by-one-click-btn" type="submit" class="to_cart">Отправить</button>
                                <div class="summary">
                                        <!-- В корзине <span class="quanty">2</span> товара на сумму: <span class="summa">14 467</span>руб -->
                                        <div>
                                            <span class="product_modal_name">
                                            </span>
                                        </div>
                                        Цена: <span class="summa product_modal_price">
                                              </span>
                                </div>
                        </form>
                </div>
        </div>
</div>
</body></html>
<section class="hits">
  <div class="container">
    <div class="row">
      <div class="col-md-24">
        <div class="with-decoration title">
          С этим товаром покупают
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-24">

        <?php if($products){ ?>
        <?php foreach ($products as $product) { ?>
        <div class="single_hit_tov">
          <figure class="tov_img">
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>"  />
          </figure>
          <div class="tov_name">
            <a><?php echo $product['name']; ?></a>
          </div>
          <?php if ($product['price']) { ?>
          <div class="price">
            <?php if (!$product['special']) { ?>
            <span><?php echo (float)$product['price']; ?></span> РУБ
            <?php } else { ?>
            <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"  style="text-decoration: line-through;color:red;"><?php echo $product['price']; ?></span>
            <?php } ?>
            <?php if ($product['tax']) { ?>
            <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
            <?php } ?>
          </div>
          <?php } ?>
          <a href="#" class="to_cart">В корзину</a>
          <div class="info_overlay">
            <a onclick="compare.add('<?php echo $product['product_id']; ?>')" class="compare">сравнить</a>
            <div class="tov_name">

              <a><?php echo $product['name']; ?></a>

            </div>
            <div class="descript">
              <?php echo $product['description']; ?>
            </div>
            <?php if (!empty($product['stock']) ){ ?>
            <div class="status nalichie">В наличии
            </div>
            <?php } else { ?>
            <div class="">
            </div>
            <?php  } ?>
            <?php if ($product['price']) { ?>
            <div class="price">
              <?php if (!$product['special']) { ?>
              <span><?php echo (float)$product['price']; ?></span> РУБ
              <?php } else { ?>
              <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"  style="text-decoration: line-through;color:red;"><?php echo $product['price']; ?></span>
              <?php } ?>
              <?php if ($product['tax']) { ?>
              <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
              <?php } ?>
            </div>
            <?php } ?>
            <a href="#one_click_buy" class="buy modal">Купить в один клик</a>
            <a onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" class="to_cart">В корзину</a>
          </div>
        </div>
        <?php } ?>
        <?php } ?>

      </div>
    </div>
  </div>
</section>

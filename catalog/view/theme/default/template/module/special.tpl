<section class="aktcii">
        <div class="container">
                <div class="row">
                        <div class="col-md-24">
                                <div class="with-decoration title">
                                        Акции
                                </div>
                        </div>
                </div>
                <div class="row">
                        <div class="col-md-14 col-xs-24">
                                <div class="row">
                                        <?php foreach ($products as $key => $product) { ?>
                                        <?php if($key != 0){ ?>
                                            <div class="col-md-12 no-padding col-xs-24 col-sm-12">
                                                    <div class="single_akt_tovar">
                                                            <div class="row">
                                                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                                                            <div class="tov_name">
                                                                                <?php echo $product['name']; ?>
                                                                            </div>
                                                                            <div class="red">Лучшая цена</div>
                                                                            <div class="price">
                                                                            <?php if ($product['price']) { ?>
                                                                              <?php if (!$product['special']) { ?>
                                                                                <span><?php echo (float)$product['price']; ?></span> руб
                                                                              <?php } else { ?>
                                                                                <span class="price-new"><?php echo (float)$product['special']; ?></span> руб
                                                                                <!--<span class="price-old"><?php echo $product['price']; ?></span>-->
                                                                              <?php } ?>
                                                                            <?php } ?>
                                                                            </div>	
                                                                            <a href="<?php echo $product['href']; ?>" class="more">подробнее</a>
                                                                    </div>
                                                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                                                            <figure class="tov_img">
                                                                                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" />
                                                                            </figure>
                                                                    </div>	
                                                            </div>
                                                            <div class="row">
                                                                    <div class="col-xs-24 hidden-sm hidden-md hidden-lg">
                                                                            <a onclick="cart.add('<?php echo $product['product_id']; ?>');" class="to_cart hidden-sm hidden-md hidden-lg">В корзину</a>
                                                                    </div>
                                                            </div>
                                                            <div class="overlay">
                                                                    <div class="clerfix top">
                                                                            <div class="overlay_title">
                                                                                <a href="<?php echo $product['href']; ?>" >
                                                                                <?php echo $product['name']; ?>
                                                                                </a>
                                                                            </div>
                                                                            <a data-toggle="" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="compare">сравнить</a>
                                                                    </div>
                                                                    <div class="overlay_price">
                                                                        <?php if ($product['price']) { ?>
                                                                            <?php if (!$product['special']) { ?>
                                                                                <span><?php echo (float)$product['price']; ?></span> руб
                                                                            <?php } else { ?>
                                                                                <span class="price-new"><?php echo (float)$product['special']; ?></span> руб
                                                                                <!--<span class="price-old"><?php echo $product['price']; ?></span>-->
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="btns_wrap">
                                                                            <a onclick="cart.add('<?php echo $product['product_id']; ?>');" class="to_cart">В корзину</a>

                                                                            <?php
                                                                             $price_h = '';
                                                                             if ($product['price']) { ?>
                                                                            <?php if (!$product['special']) { ?>
                                                                            <?php $price_h = $product['price']; ?>
                                                                            <?php } else { ?>
                                                                                <?php $price_h = $product['special']; ?>
                                                                            <?php } ?>
                                                                            <?php } ?>
                                                                            <a href="#one_click_buy" data-product_name="<?php echo $product['name']; ?>" data-product_price="<?php echo $price_h; ?>" class="buy modal">Купить в один клик</a>
                                                                    </div>
                                                            </div>								
                                                    </div>
                                            </div>
                                        <?php } ?>
                                        <?php } ?>
                                </div>
                        </div>
                        <div class="col-md-10 col-xs-24 height-100 no-padding hidden-xs col-sm-24">
                                <div class="best_offer">
                                        <?php foreach ($products as $key => $product) { ?>
                                        <?php if($key == 0){ ?>
                                        <div class="row">
                                                <div class="col-md-12 col-sm-12 col-lg-10">
                                                        <div class="tov_name">
                                                                <?= $product['name']; ?>
                                                        </div>

                                                        <?php if ($product['price']) { ?>
                                                                <?php if (!$product['special']) { ?>
                                                                        <span><?php echo $product['price']; ?>
                                                                        </span>
                                                                <?php } else { ?>
                                                                        <div class="tov_desc"><?= $product['short_description']; ?></div>
                                                                        <div class="old_price">Старая цена <?php echo $product['price']; ?></div>
                                                                        <div class="offer_price"><span><?php echo (float)$product['special']; ?></span>РУБ
                                                                        </div>
                                                                <?php } ?>
                                                        <?php } ?>
                                                        <a href="<?php echo $product['href']; ?>" class="more">Узнать подробнее</a>
                                                </div>
                                                <div class="col-md-12 col-sm-12">
                                                        <div class="label">Лучшее предложение</div>
                                                        <figure class="best_offer_img">
                                                                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>">
                                                        </figure>
                                                </div>
                                        </div>
                                        <?php } else {
                                        break;
                                        } ?>

                                        <?php } ?>
                                </div>
                        </div>
                </div>
                <div class="row">
                        <div class="col-md-24 no-padding">
                                <div class="subscribe_line">
                                        <span>Подпишитесь!</span> Узнавайте о новых акциях первым.
                                        <form id="subscribe">
                                                <input id="specials_email" name="email" type="email" placeholder="Введите Ваш e-mail">
                                                <button id="special_btn_id" type="submit">Подписаться</button>
                                        </form>
                                </div>
                        </div>
                </div>
        </div>
</section>







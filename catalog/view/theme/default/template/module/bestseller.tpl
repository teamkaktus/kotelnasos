<section class="hits">
		<div class="container">
			<div class="row">
				<div class="col-md-24">
					<div class="with-decoration title">
						Хиты продаж
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-24">
					
					<?php if($products){ ?>
                                            <?php foreach ($products as $product) { ?>
                                                    <div class="single_hit_tov">
                                                            <figure class="tov_img">
                                                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>"  />
                                                            </figure>
                                                            <div class="tov_name">
                                                                <?php if($product['product_type'] == 'service'){ ?>
                                                                    <a><?php echo $product['name']; ?></a>
                                                                <?php } else { ?>    
                                                                    <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                                                <?php } ?>
                                                            </div>
                                                            <?php if ($product['price']) { ?>
                                                                <div class="price">
                                                                    <?php if (!$product['special']) { ?>
                                                                    <span><?php echo (float)$product['price']; ?></span> РУБ
                                                                    <?php } else { ?>
                                                                    <span class="price-old" style="text-decoration: line-through;color:red;"><?php echo  (float)$product['price']; ?></span>РУБ <span class="price-new"><?php echo (float)$product['special']; ?></span>РУБ
                                                                    <?php } ?>
                                                                    <?php if ($product['tax']) { ?>
                                                                        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                                                    <?php } ?>
                                                                </div>
                                                            <?php } ?>
                                                            <a href="#" class="to_cart">В корзину</a>
                                                            <div class="info_overlay">
                                                                    <a onclick="compare.add('<?php echo $product['product_id']; ?>')" class="compare">сравнить</a>
                                                                    <div class="tov_name">
                                                                            <?php if($product['product_type'] == 'service'){ ?>
                                                                                <a><?php echo $product['name']; ?></a>
                                                                            <?php } else { ?>    
                                                                                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                                                            <?php } ?>
                                                                    </div>
                                                                    <div class="descript">
                                                                            <?php echo $product['description']; ?>
                                                                    </div>
                                                                <?php if (!empty($product['stock']) ){ ?>
                                                                    <div class="status nalichie">В наличии
                                                                    </div>
                                                                    <?php } else { ?>
                                                                        <div class="">
                                                                        </div>
                                                                <?php  } ?>
                                                                    <?php if ($product['price']) { ?>
                                                                        <div class="price">
                                                                            <?php if (!$product['special']) { ?>
                                                                            <span><?php echo (float)$product['price']; ?></span> РУБ
                                                                            <?php } else { ?>
                                                                            <span class="price-old" style="text-decoration: line-through;color:red;"><?php echo  (float)$product['price']; ?></span>РУБ <span class="price-new"><?php echo (float)$product['special']; ?></span>РУБ
                                                                            <?php } ?>
                                                                            <?php if ($product['tax']) { ?>
                                                                                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                                                            <?php } ?>
                                                                        </div>
                                                                    <?php } ?>
                                                                <a href="#one_click_buy" data-product_name="<?php echo $product['name']; ?>" data-product_price="<?php echo $price_h; ?>" class="buy modal">Купить в один клик</a>
                                                                    
                                                                       <?php
                                                                             $price_h = '';
                                                                             if ($product['price']) { ?>
                                                                            <?php if (!$product['special']) { ?>
                                                                            <?php $price_h = $product['price']; ?>
                                                                            <?php } else { ?>
                                                                                <?php $price_h = $product['special']; ?>
                                                                            <?php } ?>
                                                                            <?php } ?>

                                                                <a onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" class="to_cart">В корзину</a>
                                                            </div>
                                                    </div>
                                            <?php } ?>
                                        <?php } ?>

				</div>
			</div>
		</div>
</section>

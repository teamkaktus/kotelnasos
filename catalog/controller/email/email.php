<?php
class ControllerEmailEmail extends Controller {
	private $error = array();


	public function send() {
		$this->load->language('marketing/contact');

		$json = array();
        $prise_total = 0;
        $products = $this->cart->getProducts();
        $price_total = 0;
        	foreach ($products as $product) {
				$product_total = 0;

				foreach ($products as $product_2) {
					if ($product_2['product_id'] == $product['product_id']) {
						$product_total += $product_2['quantity'];
					}
				}

				// Display prices
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				// Display prices
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
				} else {
					$total = false;
				}


				$productss[] = array(
                    'product_id'=> $product['product_id'],
					'name'      => $product['name'],
					'quantity'  => $product['quantity'],
					'price'     => $price,
					'total'     => $total
				);
                 $price_total = $price_total + $total;
			}
            
        
        
		if ($this->request->post) {
                    $subject = 'Покупка в один клик';
                    $messagetosend = '
                    <table border="1">
                        <caption>Покупка в один клик</caption>

                                <tr>
                                <td>Имя</td><td>'.html_entity_decode($this->request->post['name']) .'</td>
                                </tr>
   
                                <tr>
                                <td>Телефон</td><td>'.html_entity_decode($this->request->post['phone']) .'</td>
                                </tr>
   
                                <tr>
                                <td>Количество</td><td>'.$product_total .'</td>
                                </tr>
                                
                                <tr>
                                <td>Общая сумма заказа</td><td>'.$price_total .'</td>
                                </tr>
   
                                </table>
                    
                    '; 
                    $messagetosend .= '
                    <table border="1">
                        <caption>Товари:</caption>
                        <tr>
                        <td>Код</td><td>Наименование</td><td>Стоимость</td><td>Кол-во</td><td>Сумма</td>
                        </tr>
                    '; 
            	foreach ($productss as $productss2) {
					 $messagetosend .= '
                                <tr>
                                <td>'.$productss2['product_id'] .'</td>
                                <td>'.$productss2['name'] .'</td>
                                <td>'.$productss2['price'] .'</td>
                                <td>'.$productss2['quantity'] .'</td>
                                <td>'.$productss2['quantity'] .'</td>
                                </tr>
                    '; 
				}
                    $messagetosend .= '
                    </table>
                    '; 
					$message  = '<html dir="ltr" lang="en">' . "\n";
					$message .= '  <head>' . "\n";
					$message .= '    <title>' . $subject . '</title>' . "\n";
					$message .= '    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
					$message .= '  </head>' . "\n";
					$message .= '  <body>' . html_entity_decode($messagetosend ) . '</body>' . "\n";
					$message .= '</html>' . "\n";
						
			
					
					

					
						
                       
					$mail = new Mail($this->config->get('config_mail'));
					$mail->setTo($this->config->get('config_email'));
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender('kotelnasos');
					$mail->setSubject($subject);
					$mail->setHtml($message);
					$mail->send();
						
					
				
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($products));
	}
}